# codeforfun-MicroService

#### 项目介绍

基于SpringCloud/Docker微服务Demo

#### 软件架构

软件架构说明

#### 安装教程

```bash
git pull https://github.com/qq253498229/codeforfun-microservice.git
cd codeforfun-microservice
docker-compose up -d
```

> 之后可以访问[http://localhost](http://localhost) 测试效果

#### 使用说明

- [点击](https://github.com/qq253498229/codeforfun-docs) 查看设计文档
- nginx/Shell/docker/jenkins整合请参考本模块
- [点击](https://github.com/qq253498229/codeforfun-front-pc) 查看前端源码



#### 参与贡献

1. Fork 本项目
2. 新建 Feature_xxx 分支
3. 提交代码
4. 新建 Pull Request

